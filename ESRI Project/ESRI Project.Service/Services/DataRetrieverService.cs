﻿using ESRI_Project.Data.Database;
using ESRI_Project.Service.Services.Contracts;
using Newtonsoft.Json;
using System.Net.Http;
using System.Threading.Tasks;
using ESRI_Project.Service.DTOs;
using Newtonsoft.Json.Linq;
using ESRI_Project.Service.Mappers;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace ESRI_Project.Service.Services
{
	public class DataRetrieverService : IDataRetrieverService
	{
		private readonly ESRIProjectDbContext _context;
		private readonly IConfiguration _configuration;
		private readonly string _clearTableSql = "TRUNCATE TABLE States";
		private readonly string _dataRetrieverUrl = "DataRetrieverServiceUrl";

		public DataRetrieverService(ESRIProjectDbContext context, IConfiguration configuration)
		{
			_context = context;
			_configuration = configuration;
		}


		/// <summary>
		/// Retrieves data from the feature layer, deserialize it and saves it to the database.
		/// </summary>
		/// <returns></returns>
		public async Task RetrieveAndSaveData()
		{
			_context.Database.ExecuteSqlRaw(_clearTableSql);
			
			var _url = _configuration[_dataRetrieverUrl];

			using (var httpClient = new HttpClient())
			{

				var response = await httpClient.GetStringAsync(_url);
				var featureLayer = (JObject)JsonConvert.DeserializeObject(response);
				var features = featureLayer["features"].Value<JArray>();
				
				foreach (var feature in features)
				{
					var attribute = feature.ToObject<AttributeDTO>();
					var state = attribute.Attributes.ToModel();
					await _context.AddAsync(state);
				}

				await _context.SaveChangesAsync();
			}
		}
	}
}
