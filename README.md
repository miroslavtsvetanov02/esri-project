# Description

Solution that fetches data from the ESRI WebService, updates it daily, allowing users to access accurate data about different states population.

# Technologies

- Entity Framework Core
- ASP .NET Core API
- Swagger
- MS SQL Server
- Hangfire
- Newtonsoft