using ESRI_Project.Data.Database;
using ESRI_Project.Service.Services;
using ESRI_Project.Service.Services.Contracts;
using Hangfire;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;

namespace ESRI_Project
{
	public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			
			services.AddDbContext<ESRIProjectDbContext>(options =>
				options.UseSqlServer(
					Configuration.GetConnectionString("DefaultConnection")));

			services.AddHangfire(config =>
		        config.UseSqlServerStorage(
			        Configuration.GetConnectionString("DefaultConnection")));
			

			//Services
			services.AddTransient<IDataRetrieverService, DataRetrieverService>();
			services.AddScoped<IStateService, StateService>();

			

			services.AddMvcCore()
		            .AddApiExplorer();
			
			services.AddControllers();

			services.AddRazorPages();
			services.AddSwaggerGen(c =>
			{
				c.SwaggerDoc("v1", new OpenApiInfo
				{
					Version = "V1",
					Title = "ESRI Project"
				});
			});
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
		{

			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}
			else
			{
				app.UseExceptionHandler("/Error");
			}

			app.UseStaticFiles();

			app.UseRouting();

			app.UseAuthorization();

			app.UseHangfireServer();
			app.UseHangfireDashboard();

			//Background process to retrieve data from API and save it to database
			RecurringJob.AddOrUpdate<IDataRetrieverService>(
				x => x.RetrieveAndSaveData(),
				Cron.Daily);

			RecurringJob.TriggerJob("IDataRetrieverService.RetrieveAndSaveData");


			app.UseSwagger();
			app.UseSwaggerUI(c =>
			{
				c.SwaggerEndpoint("/swagger/v1/swagger.json", "ESRI Project API");
			});

			app.UseEndpoints(endpoints =>
			{
				endpoints.MapControllers();
			});
		}
	}
}
