﻿using ESRI_Project.Data.Database;
using ESRI_Project.Service.DTOs;
using ESRI_Project.Service.Mappers;
using ESRI_Project.Service.Services.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ESRI_Project.Service.Services
{
	public class StateService : IStateService
	{
		private readonly ESRIProjectDbContext _context;

		public StateService(ESRIProjectDbContext context)
		{
			_context = context;
		}

		/// <summary>
		/// Gets all states from the database.
		/// </summary>
		/// <returns></returns>
		public async Task<List<StateDTO>> GetAllAsync()
		{
			var states = await _context.States
				.Select(x => x.ToDTO())
				.ToListAsync();
			
			return states;
		}

		/// <summary>
		/// Gets a state by name from the database.
		/// </summary>
		/// <param name="name"></param>
		/// <returns></returns>
		/// <exception cref="ArgumentException"></exception>
		public async Task<List<StateDTO>> GetByNameAsync(string name)
		{
			var states = await _context.States
				.Where(x => x.StateName == name)
				.Select(x => x.ToDTO())
				.ToListAsync();

			return states;
		}
	}
}
