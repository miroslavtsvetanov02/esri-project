﻿using ESRI_Project.Service.Services.Contracts;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System.Threading.Tasks;

namespace ESRI_Project.APIControllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class StatesController : ControllerBase
	{
		private readonly IStateService _StateService;

		public StatesController(IStateService featureLayerService)
		{
			_StateService = featureLayerService;
		}

		[HttpGet]
		[SwaggerOperation(Summary = "Get all states", Description = "Get all states from the database")]
		public async Task<IActionResult> GetAllAsync()
		{
			var states = await _StateService.GetAllAsync();

			return Ok(states);

		}

		[HttpGet("{name}")]
		[SwaggerOperation(Summary = "Get state by name", Description = "Get state by name from the database")]
		public async Task<IActionResult> GetByNameAsync(string name)
		{

			var states = await _StateService.GetByNameAsync(name);

			switch (states.Count)
			{
				case 0:
					return NotFound();

				default:
					return Ok(states);
			}
		}
	}
}
