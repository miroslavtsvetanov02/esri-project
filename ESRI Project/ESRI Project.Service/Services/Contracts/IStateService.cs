﻿using ESRI_Project.Service.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ESRI_Project.Service.Services.Contracts
{
	public interface IStateService
	{
		Task<List<StateDTO>> GetAllAsync();
		Task<List<StateDTO>> GetByNameAsync(string name);
	}
}
