﻿using System.Threading.Tasks;

namespace ESRI_Project.Service.Services.Contracts
{
	public interface IDataRetrieverService
	{
		Task RetrieveAndSaveData();
	}
}
