﻿using ESRI_Project.Data.Models;
using ESRI_Project.Service.DTOs;

namespace ESRI_Project.Service.Mappers
{
	public static class StateMapper
	{
		public static State ToModel(this StateDTO state)
		{
			return new State
			{
				Population = state.Population,
				StateName = state.State_Name,
			};
		}

		public static StateDTO ToDTO(this State state)
		{
			return new StateDTO
			{
				Population = state.Population,
				State_Name = state.StateName,
			};
		}
	}
}
