﻿using ESRI_Project.Data.Database.Contracts;
using ESRI_Project.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace ESRI_Project.Data.Database
{
	public class ESRIProjectDbContext : DbContext, IDatabase
	{

		public ESRIProjectDbContext(DbContextOptions<ESRIProjectDbContext> options) 
			: base(options)
		{
		}
		
		public DbSet<State> States { get; set; }

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{	
			base.OnModelCreating(modelBuilder);
		}
	}
}
