﻿using ESRI_Project.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace ESRI_Project.Data.Database.Contracts
{
	public interface IDatabase
	{
		public DbSet<State> States { get; set; }
	}
}
