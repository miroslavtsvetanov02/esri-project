﻿namespace ESRI_Project.Data.Models
{
	public class State
	{
		public int Id { get; set; }
		public int Population { get; set; }
		public string StateName { get; set; }
	}
}
